using System.Collections.Generic;
using UnityEngine;

public class GameEnvironment : MonoBehaviour
{
    private static GameEnvironment _instance;
    private List<GameObject> checkPoints = new List<GameObject>();
    public  List<GameObject> CheckPoints
    {
        get { return checkPoints; }
    }

    public static GameEnvironment Singleton
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GameEnvironment();
                _instance.CheckPoints.AddRange(
                    GameObject.FindGameObjectsWithTag("Checkpoint"));
                
            }

            return _instance;
        }
    }
}
