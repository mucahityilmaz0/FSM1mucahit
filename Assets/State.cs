using UnityEngine;
using UnityEngine.AI;

public class State
{
    public enum STATE
    {
        IDLE, PATROL, PURSUE, ATTACK, SLEEP
    };

    public enum EVENT
    {
        ENTER, UPDATE, EXIT
    };

    public STATE name;
    protected EVENT stage;
    protected GameObject npc;
    protected Animator anim;
    protected Transform player;
    protected State nextState;
    protected NavMeshAgent agent;

    float visDist = 10f;
    float visAngle = 30f;
    float shootDist = 7f;

    public State(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player)
    {
        npc = _npc;
        agent = _agent;
        anim = _anim;
        stage = EVENT.ENTER;
        player = _player;
    }
    
    public virtual void Enter() { stage = EVENT.UPDATE; }
    public virtual void Update() { stage = EVENT.UPDATE; }
    public virtual void Exit() { stage = EVENT.EXIT; }

    public State Process()
    {
        if (stage == EVENT.ENTER) Enter();
        if (stage == EVENT.UPDATE) Update();
        if (stage == EVENT.EXIT)
        {
            Exit();
            return nextState;
        }
        return this;
    }
}

public class Idle : State
{
    public Idle(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player)
                :base(_npc,_agent,_anim,_player)
    {
        name = STATE.IDLE;
    }
    public override void Enter()
    {
        anim.SetTrigger("isIdle");
        base.Enter();
    }

    public override void Update()
    {
        if (Random.Range(0,100) < 10)
        {
            nextState = new Patrol(npc, agent, anim, player);
        }
        base.Update();
    }
    public override void Exit()
    {
        anim.ResetTrigger("isIdle");
        base.Exit();
    }
}

public class Patrol : State
{
    public Patrol(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player)
        :base(_npc,_agent,_anim,_player)
    {
        name = STATE.PATROL;
    }
    public override void Enter()
    {
        anim.SetTrigger("isPatrol");
        base.Enter();
    }

    /*public override void Update()
    {
        if (Random.Range(0,100) < 10)
        {
            nextState = new Patrol(npc, agent, anim, player);
        }
        base.Update();
    }*/
    public override void Exit()
    {
        anim.ResetTrigger("isPatrol");
        base.Exit();
    }
}